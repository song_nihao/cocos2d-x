LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := cocos_cares_static
LOCAL_MODULE_FILENAME := cares
LOCAL_SRC_FILES := libs/$(TARGET_ARCH_ABI)/libcares.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
include $(PREBUILT_STATIC_LIBRARY)
